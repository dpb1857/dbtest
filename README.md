
** Install Prerequisites
  On a macbook:
    brew install golang-migrate

** Create a New Schema Migration

```
cd migrations
cd <database-name>  # example: clublocal
cd <env>            # common==all environments, or dev, prod for environment-specific migrations;
migrate create -ext .sql <change-descripion>
```

This will create two files -
```
20201124110810_testing.down.sql
20201124110810_testing.up.sql
```

Add your up and down migrations to these files.

** Run the Migration

From the top-level, run the script:

```
scripts/migrate.sh
```

** Database URL



** Questions

* Where do database credentials live?
  a) protected in the Gitlab repo ci/cd variables;
  b) credentials are available or builtin to the pipeline runner;
     -> compiled into the image somehow;
     -> included in instance metadata;
     -> access available via role assignments;
