#!/bin/bash

# This script serves as the main entrypoint for the container.

function help() {
    echo "Commands:" 1>&2
    echo "  shell:  start a shell" 1>&2
    echo "  run: start gitlab runner" 1>&2
    echo
    echo "    required environment variables:" 1>&2
    echo "    TOKEN - gitlab runner registration token (Settings/ CI/CD / Runners)" 1>&2
    echo "    NAME  - runner display name on CI/CD page" 1>&2
    echo "    DBURL - db access url (e.g., mysql://user:passwd@tcp(hostname:3306)/dbname)" 1>&2
    echo "    ENV   - environment (e.g., dev, stage, prod,...); tag applied to the runner." 1>&2
    exit 1
}

function error() {
    echo $1 1>&2
    exit 1
}

function runner() {
    if [ "$TOKEN" = "" ]; then
        error "environment variable TOKEN is missing"
    fi
    if [ "$NAME" = "" ]; then
        error "environment variable NAME is missing"
    fi
    if [ "$DBURL" = "" ]; then
        error "environment variable DBURL is missing"
    fi
    if [ "$ENV" = "" ]; then
        error "environment variable ENV is missing"
    fi

    # Extract the host:port from the DBURL variable
    hostport=`echo $DBURL|sed 's/.*(\(.*\)).*/\1/'`

    # Make sure we can get there from here
    dbhost=`echo $hostport|sed 's/:.*//'`
    dbport=`echo $hostport|sed 's/.*://'`
    echo "checking database connection"
    if ! nc -vz $dbhost $dbport 2>/dev/null; then
        error "Cannot connect to database at $hostport"
    fi

    echo "Using database at $hostport"
    echo "Register runner"
    gitlab-runner register --non-interactive \
                  --name "$NAME" \
                  --url "https://gitlab.com" \
                  --registration-token "$TOKEN" \
                  --executor "shell" \
                  --shell="bash" \
                  --tag-list "$ENV"

    REGISTERED=1
    echo "Starting runner"
    gitlab-runner run "$@"
    echo "exited"
}

function exithandler() {
    if [ $REGISTERED -eq 1 ]; then
        echo "Unregister runner"
        gitlab-runner unregister --all-runners
    fi
    echo "exiting"
}

trap exithandler EXIT

REGISTERED=0
cmd=$1
shift
case $cmd in
    help)
        help
        ;;
    run)
        runner "$@"
        ;;
    shell)
        exec /bin/sh
        ;;
    *)
        help
        ;;
esac
