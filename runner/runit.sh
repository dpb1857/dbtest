#!/bin/bash

# Sample invocation of the runner container.

# Connect to local mysql instance
export DBURL="mysql://devuser:devpassword@tcp(host.docker.internal:3306)/clublocal"

set -x
docker run -it --network host -e ENV=dev -e NAME=macbook-docker -e TOKEN=_d6YerHTSA-txRxHvd93 -e DBURL="$DBURL" --rm runner run
