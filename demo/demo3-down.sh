#!/bin/sh

set -x
migrate -source file:dev -database $DBURL down "$@"
