export DBURL='mysql://devuser:devpassword@tcp(127.0.0.1:3306)/clublocal'
export DBHOST=127.0.0.1
export DBNAME=clublocal
export USER=devuser
export MYSQL_PWD=devpassword
export ENV="dev"
