#!/bin/bash

# This script combines common migrations with environment-specific migrations
# and applies them.

function validate_dburl() {
    DBURL=${DBURL:?"DBURL not set"}

    # Extract the host:port from the DBURL variable
    hostport=$(echo $DBURL|sed 's/.*(\(.*\)).*/\1/')

    # Make sure we can get there from here
    dbhost=$(echo $hostport|sed 's/:.*//')
    dbport=$(echo $hostport|sed 's/.*://')
    database=$(echo $DBURL|sed 's/.*\///')
    echo -n "checking database connection at ${dbhost}:${dbport}... "
    if ! nc -vz $dbhost $dbport 2>/dev/null; then
        echo "Cannot connect to database at $hostport" 1>&2
        exit 1
    fi
    echo "ok."
}

function validate_env() {
    if [ "${ENV}" != "dev" -a "${ENV}" != "prod" ]; then
        echo 'ENV must be set to "dev" or "prod".' 1>&2
        echo "Current value: ${ENV:-unset}" 1>&2
        exit 2
    fi
}

function get_version() {
  version=$(migrate -path merged -database ${DBURL} version 2>&1|sed 's/error: no migration//')
  echo ${version:--1}
}

# Wrap the migrate up functionality so that on error, we reset to the last working migration.
function migrate_up() {
    current=$(get_version)
    echo "Starting db revision: ${current}"
    out=$(migrate -path merged -database ${DBURL} $subcommand "$@" 2>&1)
    if [ $? -ne 0 ]; then
        echo "Migration failed: $(get_version)"
        last_working=$(echo "$out" | egrep '^[0-9]+/u' | tail -1 | cut -f1 -d'/')
        last_working=${last_working:=${current}}
        echo "Resetting version to ${last_working}"
        migrate -path merged -database ${DBURL} force ${last_working}
        echo
        echo "Command output:"
        echo "$out"
        echo
        echo "Keeping merged/ directory for debugging."
        exit 1
    fi

    echo "$out"
}

# For now, get the DBURL should be in the container environment.
# We may cosider getting it or constructing it from Gitlab job env variables,
# or perhaps the container will have its own methods for getting
# configuration information.

DBURL=${DBURL}
validate_dburl
validate_env

echo "HOST: $dbhost PORT: $dbport DATABASE: $database ENV:$ENV "

if [ ! -d migrations/${database} ]; then
    echo "Migrations directory for database ${database}, migrations/${database}, does not exist." 1>&2
    exit 3
fi

rm -rf merged
mkdir merged

if [ -d migrations/${database}/common -a "$(ls migrations/${database}/common 2>/dev/null)" ]; then
    cp migrations/${database}/common/*.sql merged
fi

if [ -d migrations/${database}/${ENV} -a "$(ls migrations/${database}/${ENV} 2>/dev/null)" ]; then
    cp migrations/${database}/${ENV}/*.sql merged
fi

subcommand=${1:-up}
shift

if [ "$subcommand" = "up" ]; then
    migrate_up "$@"
else
    migrate -path merged -database ${DBURL} $subcommand "$@"
fi
rm -rf merged
